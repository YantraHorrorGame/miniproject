extends Spatial

var timer = 0

func _ready():
	$HorrorWall.visible = false;
	$Theme.play()


func _process(delta):
	if $"/root/HUD".orb >= 1:
		$HorrorWall.visible = true;
		$Wall.visible = false;
		blinker()

func blinker():
	$OmniLight.light_energy -= 0.3
	$OmniLight2.light_energy -= 0.3
	$OmniLight3.light_energy -= 0.3
	$OmniLight4.light_energy -= 0.3
	$OmniLight5.light_energy -= 0.3
	$OmniLight6.light_energy -= 0.3
	$OmniLight7.light_energy -= 0.3
	$OmniLight8.light_energy -= 0.3
	$OmniLight9.light_energy -= 0.3
	$OmniLight10.light_energy -= 0.3
	yield(get_tree().create_timer(0.1), "timeout")
	if $OmniLight4.light_energy >= 0:
		blinker()
	else:
		light()

func light():
	$OmniLight.light_energy = 1
	$OmniLight2.light_energy = 1
	$OmniLight3.light_energy = 1
	$OmniLight4.light_energy = 1
	$OmniLight5.light_energy = 1
	$OmniLight6.light_energy = 1
	$OmniLight7.light_energy = 1
	$OmniLight8.light_energy = 1
	$OmniLight9.light_energy = 1
	$OmniLight10.light_energy = 1