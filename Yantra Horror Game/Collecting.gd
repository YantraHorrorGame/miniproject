extends Spatial



func _ready():
	$GlowingOrb2/Area/CollisionShape.disabled = true;
	$GlowingOrb3/Area/CollisionShape.disabled = true;
	$GlowingOrb4/Area/CollisionShape.disabled = true;
	$GlowingOrb5/Area/CollisionShape.disabled = true;

func _process(delta):
	if $"/root/HUD".orb == 1 :
		$GlowingOrb2.visible = true;
		$GlowingOrb2/Area/CollisionShape.disabled = false;
	if $"/root/HUD".orb == 2 :
		$GlowingOrb3.visible = true;
		$GlowingOrb3/Area/CollisionShape.disabled = false;
	if $"/root/HUD".orb == 3 :
		$GlowingOrb4.visible = true;
		$GlowingOrb4/Area/CollisionShape.disabled = false;
	if $"/root/HUD".orb == 4 :
		$GlowingOrb5.visible = true;
		$GlowingOrb5/Area/CollisionShape.disabled = false;