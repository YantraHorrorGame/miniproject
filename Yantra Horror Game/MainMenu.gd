extends Control

func _ready():
	$"/root/HUD/Orb".visible = false;
	$"/root/HUD/Time".visible = false;

func _on_Button_pressed():
	$"/root/HUD/Orb".visible = true;
	$"/root/HUD/Time".visible = true;
	$"/root/HUD".orb = 0
	$"/root/HUD".time = 180
	$"/root/HUD".start = true
	get_tree().change_scene("res://Level1.tscn")