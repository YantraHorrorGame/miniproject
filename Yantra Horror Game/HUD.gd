extends CanvasLayer

var orb = 0
var time = 0
var start = false

func _ready():
	pass

func _process(delta):
	$Orb.text = "Orb: " + str(orb) + "/5"
	$Time.text = "Time: " + str(int(time)) + "s"

	if start == true:
		time -= delta

	if time < 0:
		time = 60
		start = false
		get_tree().change_scene("res://GameOver.tscn")
	
	if orb == 5:
		start = false
		get_tree().change_scene("res://GameOver.tscn")